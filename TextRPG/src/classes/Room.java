package classes;

import java.util.ArrayList;

import extlib.TextIO;

public class Room {

	private ArrayList<Item> objects = new ArrayList<Item>();
	private String description;
	
	public Room (String d) {
		
		this.description = d;
        

		
	}
	

	
	public void printDescription () {
		
		System.out.println(this.description);
		
		
	}
	
	/* Item pickup using the arrayList*/
	
    public Item pickUp() {
    	printObjects();
    	System.out.print("What would you like to pick up?");
    	int itemChoice =TextIO.getInt();
    	if (itemChoice > 0 && itemChoice <= objects.size()){
    		Item item1 = objects.get(itemChoice-1);
    		objects.remove(item1);
    		return (item1);
    		
    		
    	} else {
    		System.out.println("Invalid choice.");
    		return pickUp();
    		
    		
    		
    		
    		
    	}
    }
     /* Checks if room has object to use or interact with */
    public boolean hasObject() {
    	if (objects.isEmpty()){
            	return false;
    	} else {
    		return true;
    	}
    }
	
    /*Prints out all the objects in the room*/
    
	public void printObjects() {
        System.out.println("Items you can use: ");
		for(int i=0;i<objects.size();i++) {
			
			Item item = objects.get(i);
			System.out.println(i + " " + item.getName());
    		    

		}
		if(objects.isEmpty()) {
			System.out.println("Room is empty?");
		}
		
		
		
	}
	
	
    
	
}

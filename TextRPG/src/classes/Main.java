package classes;

import extlib.TextIO;

public class Main {

	private static Map map = new Map(); 
	private static Player player = new Player();
	
	private static void move() {
		//Checks wether you can go in the direction, restrictions are in MAP class.//
		boolean north = map.isValidLocation(player.getX(),player.getY()+1);
		boolean south = map.isValidLocation(player.getX(),player.getY()-1);
		boolean east = map.isValidLocation(player.getX()+1,player.getY());
		boolean west = map.isValidLocation(player.getX()-1,player.getY());
		System.out.println("In which direction would you like to go?");
        
		/* Prints out options what to do, based off of the boolean value */
        if (north) {
			
			System.out.println("North");
			
		}
		if (south) {
			System.out.println("South");
			
		}
		
		if (east) {
			
			System.out.println("East");
			
			
		}
		
		if (west) {
			System.out.println("West");
			
		}
        /* A loop that goes on forever*/
		
		while(true) {
			String userInput = TextIO.getlnString().toLowerCase();
	        
			if(userInput.equals("north") && north)  {
				player.setY(player.getY()+1);
				System.out.println("You traversed North");
				break;
			} else if (userInput.equals("south")&& south)  {
				player.setY(player.getY()-1);
				System.out.println("You went South");
				break;
			} else if (userInput.equals("east") && east)  {
				player.setX(player.getX()+1);
				System.out.println("You jumped East");
				break;
			} else if (userInput.equals("west") && west)  {
				player.setX(player.getX()-1);
				System.out.println("You ran West");
				break;
			} else {
				System.out.println("Invalid selection");
			}
		
		}
		/* Creates the rooms */
		
		Room room = map.getRoom(player.getX(),player.getY());
		room.printDescription();
        room.printObjects();
		
	}
 
    public static void printOptions() {
    	System.out.println("Note, some of these are applicable in certain rooms");
    	System.out.println("Options: use,(inv)entory,stats,move,gear,quit,get,scan,throw");
    }
	public static void main(String[] args) { 
		
						
		player.startUp();

        /* A loop that goes on for as long as true goes to false, 
         * and the condition is if player is alive.
         * The three booleans are what I use to have some action t
         * take place in the rooms using specific coordinates for the rooms,
         * where you can move around using keywords.
         */
		
				
		while(player.alive()) {
			System.out.println("What is it you want to do?");
			printOptions();
			String decision = TextIO.getlnString().toLowerCase();
			Room room = map.getRoom(player.getX(), player.getY());
			boolean treasureRoom = player.getX() == 2 && player.getY() == 1;
			boolean gargoyle = player.getX() == 2 && player.getY() == 2;
			boolean endGame = player.getX() == 1 && player.getY() == 2;
			if(decision.equals("move")) {
				move();
			    if (player.getX() == 2 && player.getY() == 1) {
				System.out.println("You have found the treasure room");
			    						
			    } else if (player.getX() == 2 && player.getY() == 2) {
			    	System.out.println("There is a large gargoyle statue in the middle");
			    	
			    } else if (player.getX() == 1 && player.getY() == 2) {
			    	System.out.println("There is a locked door, a knife at the doorknob.");
			    }
			} else if (decision.equals("stats")){
				player.printStats();
			} else if(decision.equals("gear")) {
				player.printGear();
			} else if (decision.equals("inventory") || decision.equals("inv")){
				player.printInventory();
			} else if (decision.equals("quit")) {
				System.out.println("This game was made by Andris M�nnik (c) 2016");
				return;
			} else if (decision.equals("use")){
				player.use();
			} else if(decision.equals("get") && treasureRoom) {
				player.loseHealth(5);
				System.out.println("A rock falls on you, try something else");
			} else if (decision.equals("scan") && treasureRoom) {
				System.out.println("There's a bunch of rocks suspended in the air,held up there with a stick");
			} else if (decision.equals("throw rock") && treasureRoom) {
				System.out.println("The rock thrown is flung at the stick,it falls down");
			} else if (decision.equals("hug") && gargoyle) {
				System.out.print("You have hugged the gargoyle,the evil statue sneers,dark magic healing you");
				player.heal(10);
			} else if (decision.equals("get") && room.hasObject() ){
				Item x =room.pickUp();
				player.take(x);
				
				
			} else if(decision.equals("use knife") && endGame) {
				player.loseHealth(10);
				player.doorHealth(10);
				if(player.getSacrificedHealth() == 50) {
				    System.out.println("The door has opened,you are free to leave!");	
				}
				
				
			} else {
				System.out.println("Try again.");
			}
			
		} 
		
		System.out.println("You died.");
		
        
        
        

        	
    }
			
		
		
		    
		
	
	
}





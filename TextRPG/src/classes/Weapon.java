package classes;

public class Weapon {
    
	private int damage;
	private String description;
	
	

	public Weapon (int damage,String description) {
		
		this.damage = damage;
		this.description = description;
		
		
	}
	
	public void printStats() {
		
		System.out.println("damage: "+this.damage);
		
	}
	
	public void printDescription() {
		
		System.out.println(this.description);
		
	}
	
	
}



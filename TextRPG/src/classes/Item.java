package classes;

public abstract class Item {
    private String description;
    private String name;

    public Item(String description, String name){
    	this.description = description;
    	this.name = name;
    }
    
    abstract boolean canUse();
    abstract void use(Player x);

    public String getDescription() {
    	return this.description;
    }
    
    public String getName() {
    	return this.name;
    }
}



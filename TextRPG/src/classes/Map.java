package classes;

public class Map {
	private static final int minX = -1;
	private static final int maxX = 3;
	private static final int minY = -1;
	private static final int maxY = 3;
	
	Room[] top = {
		new Room ("The forest is damp and dark,not a living soul in sight,a mosquito lands on your neck,"
				+ "starting to suck blood"),
		new Room ("Gets darker and darker,unable to see where you're really going."),
		new Room ("Nothing really here, a screeching voice in the dark mist, a monster approaching you.")
	};
	
	Room[] middle = {
		new Room ("You enter the stone graveyard mausoleum in the forest,the place is dilapitated and "
				+ "in ruins"),
		new Room ("The earth rumbles, bats flying around screeching,attacking you."),
		new Room ("You arrive in a mausoleum, your family crest and insignia on the stone slab outside")
	};
	
	Room[] bottom = {
		new Room ("There are no signs of life in this area, a single cow skull resting on a fence post"),
		new Room ("A clear sacrificial area, the corpse of the cow in the middle of a stone slab"),
		new Room ("This area has nothing interesting in it.")
	};

    public Room getRoom(int x,int y){
    	
    	if (isValidLocation(x,y)) {
    		if (y == 2) {
    			return top[x];
    		} else if (y == 1) {
    			return middle[x];
    		} else {
    			return bottom[x];
    		}
    	} else {
    		throw new IndexOutOfBoundsException("Coordinate is not valid.");
    	}	
    }
		
	
	public boolean isValidLocation(int mapX, int mapY) {
		
		
		if (mapX >= maxX || mapX <= minX) {
			return false;
		} else if (mapY >= maxY || mapY <= minY){
			return false;
		} else {
			return true;
		}
		
		
	}

}




package classes;

import java.util.*;



import extlib.TextIO;

public class Player {
	
	private ArrayList<Item> inventory = new ArrayList<Item>();
	
		

	private String name;
	private int intellect;
	private int strength;
	private int agility;
	private int charisma;
    private int health;
    private int mana;
    private int sacrifiedHealth;
    
    private int mapX;
    private int mapY;
    private Weapon weapon;
    
    public Player () {
    	
    	this.agility = 1;
    	this.strength = 1;
    	this.intellect = 1;
    	this.charisma = 1;
    	this.health= 10;
    	this.mana = 5;
    	this.mapX = 0;
    	this.mapY = 0;
    	this.weapon = new RustyKitchenSpoon();	
    	this.inventory.add(new HealthPotion());
    	this.inventory.add(new MegaHealthPotion());
        this.sacrifiedHealth = 0;
    	
    }
    
    public int getSacrificedHealth() {
    	return this.sacrifiedHealth;
    }
    
    public int getX() {
    	return this.mapX;
    }
    
    public int getY() {
    	return this.mapY;
    }
    
    public void setX(int x) {
    	this.mapX = x;
    }
    
    public void setY(int y) {
    	this.mapY = y;
    }
    
    public void sacrificedHealth(int sacrifiedHealth) {
    	this.sacrifiedHealth = sacrifiedHealth;
    }
   public void printStats () {
	   
	   System.out.println("Strength: " + this.strength);
	   System.out.println("Intellect: " + this.intellect);
	   System.out.println("Agility: " + this.agility);
	   System.out.println("Charisma: " + this.charisma);
	   System.out.println("Health: " + this.health);
	   System.out.println("Mana: " + this.mana);
	   
	   
   } 
    
   
   public void printGear() {
	   
	   System.out.println("I am wielding a ");
	   this.weapon.printDescription();
	   
   }
   
    
    
	public void startUp() {
		
		System.out.println("Welcome to the game,please enter your name:");
		
		name = TextIO.getlnString();
		
		
		
	}
	
	public void doorHealth(int amount) {
		this.sacrifiedHealth = this.sacrifiedHealth +amount;
	}
	
	public void heal(int amount) {
		System.out.println("You've been healed by " +amount);
        this.health = this.health+amount;		
		
	}
	
    public void loseHealth(int amount) {
    	System.out.println("You've lost " +amount + " health");
    	this.health= this.health-amount;
    }
	
	public boolean alive() {
		return this.health > 0;
	}

	public void printInventory() {
		
		if (inventory.size() == 0) {
			System.out.println("You aren't holding anything.");
			return;
		}
		
		System.out.println("You are holding ");
		
		for (int i = 0; i < inventory.size(); i++){
			System.out.println(inventory.get(i).getName());
		}

		System.out.println();
	}
	
	public void use(){
		for (int i = 0; i < inventory.size(); i++){
			System.out.println(i+1 + " " + inventory.get(i).getName());
			
		}
		System.out.println("Which item do you want to use?");
		int choice = TextIO.getInt();
		
		if(choice <= 0 || choice > inventory.size()){
			use();
		} else {
			Item item = inventory.get(choice-1);
			if(!item.canUse()) {
				System.out.println("You can't use this");
				return;
			}
			item.use(this);
			
			inventory.remove(choice-1);
			
      
	        
		}   
	}
	
	
	
	 public void take (Item x) {
		 inventory.add(x);
	 } 
	
}
